import mongoose from 'mongoose'

export const findAll = async (Model, res) => {
  try {
    const itemsFound = await Model.find()
    res.status(200).json({ results: itemsFound})
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
}

export const deleteById = async (Model, res, id) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No document with id: ${id}`);

    await Model.findByIdAndRemove(id);

    res.json({ message: "Document deleted" });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
}

export const getById = async (Model, res, searchObj) => {
  try {
    const results = await Model.find(searchObj)

    res.status(200).json({ results })
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
}

export const update = async (Model, res, updateObject, id) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`Document not found`)

    await Model.findByIdAndUpdate(id, updateObject, { new: true })

    res.status(200).json({ results: updateObject })
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" })
    
    console.log(error)
  } 
}

export const create = async (Model, res, newModelObject) => {
  const newDocument = new Model({...newModelObject})
  try {
    await newDocument.save()

    res.status(201).json({ result: newDocument })
  } catch (error) {
    res.status(404).json({ message: error.message });

  }
}