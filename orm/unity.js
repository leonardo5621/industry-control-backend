import mongoose from 'mongoose'

const unitySchema = mongoose.Schema({
  name: { type: String, required: true},
  machines: [{type: mongoose.Schema.Types.ObjectId, ref: "Machine"}],
  company: { type: mongoose.Schema.Types.ObjectId, ref: "Company"},
  maintainer: { type: mongoose.Schema.Types.ObjectId, ref: "User"}
})

export default mongoose.model("Unity", unitySchema)