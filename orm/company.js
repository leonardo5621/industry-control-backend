import mongoose from 'mongoose'

const companySchema = mongoose.Schema({
  name: { type: String, required:  true },
  creationDate: { type: Date, required: true},
  creator: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
})

export default mongoose.model("Company", companySchema)