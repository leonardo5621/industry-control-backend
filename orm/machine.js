import mongoose from 'mongoose'

const machineSchema = mongoose.Schema({
  name: { type: String, required: true},
  model: { type: String, required: true},
  maintainer: { type: mongoose.Schema.Types.ObjectId, ref:"User" ,required: true},
  status: [{ type: Object }],
  healthLevel: [{ type: Object }],
  description: { type: String},
  unity: { type: mongoose.Schema.Types.ObjectId, ref: "Unity"},
  image: String,
})

export default mongoose.model("Machine", machineSchema)