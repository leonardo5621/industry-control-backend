import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import mongoose from 'mongoose'
import dotenv from 'dotenv'
import machinesRoutes from './routes/machines.js'
import unitiesRoutes from './routes/unities.js'
import companiesRoutes from './routes/companies.js'
import usersRoutes from './routes/users.js'

dotenv.config()

const app = express()

app.use(bodyParser.json({ limit: '30mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }))
app.use(cors())

app.use('/machines', machinesRoutes)
app.use('/unities', unitiesRoutes)
app.use('/companies', companiesRoutes)
app.use('/users', usersRoutes)


const connectUrl = 'mongodb+srv://leonardo:12345@cluster0.yqrpt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
const PORT = process.env.PORT || 5000

mongoose.connect(connectUrl, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    app.listen(PORT, () => {
      console.log('server running')
    })
  })
  .catch((error) =>  console.log(error.message))

mongoose.set('useFindAndModify', false)