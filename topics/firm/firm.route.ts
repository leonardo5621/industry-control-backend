import express from 'express'
import { getCompanies, createCompany, deleteCompany } from '../controllers/companies.js'

const router = express.Router()

router.get('/', getCompanies)
router.post('/', createCompany)
router.delete('/:id', deleteCompany)


export default router