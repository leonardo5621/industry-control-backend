import Company from '../orm/company.js'
import { deleteById, create, getById, findAll } from './utils.js'

export const getCompanies = (req, res) => findAll(Company, res)

export const createCompany = (req, res) => create(Company, res, req.body)

export const deleteCompany = (req, res) => deleteById(Company, res, req.params.id)

