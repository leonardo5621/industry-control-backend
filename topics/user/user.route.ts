import express from 'express'
import { getUserByCompany, createUser, getAllUsers, updateUser, deleteUser } from '../controllers/users.js'

const router = express.Router()

router.get('/', getAllUsers)
router.get('/:id', getUserByCompany)
router.post('/', createUser)
router.patch('/:id', updateUser)
router.delete('/:id', deleteUser)

export default router