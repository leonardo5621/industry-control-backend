import mongoose from 'mongoose'
import UserModel from '../orm/user.js.js'
import { findAll, getById, update, deleteById } from './utils.js'

export const getAllUsers = (req, res) => findAll(UserModel, res) 

export const getUserByCompany = (req, res) => getById(UserModel, res, { company: req.params.id})


export const createUser = async (req, res) => {
  const userData = req.body;
  try {
    const existentUser = await UserModel.findOne({ email: userData?.email })

    if (existentUser) return res.status(400).json({ message: "User already exists" })

    const result = await UserModel.create({ ...userData })

    res.status(201).json({ result })
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" })
    
    console.log(error);
  }
};

export const updateUser = (req, res) => update(UserModel, res, req.body, req.params.id)

export const deleteUser = (req, res) => deleteById(UserModel, res, req.params.id)