import express from 'express'
import { getMachines, createMachine, getMachineByUnity, updateMachine, deleteMachine } from '../controllers/machines.js'

const router = express.Router()

router.get('/', getMachines)
router.post('/', createMachine)
router.get('/:id', getMachineByUnity)
router.patch('/:id', updateMachine )
router.delete('/:id', deleteMachine)


export default router