import Machine from '../orm/machine.js.js'
import { create, deleteById, findAll, getById, update } from './utils.js'

export const getMachines = async (req, res) => findAll(Machine, res)

export const createMachine = (req, res) => create(Machine, res, req.body)

export const getMachineByUnity = (req, res) => getById(Machine, res, { unity: req.params.id })

export const updateMachine = (req, res) => update(Machine, res, req.body, req.params.id ) 

export const deleteMachine = (req, res) => deleteById(Machine, res, req.params.id)