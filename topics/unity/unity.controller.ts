import Unity from '../orm/unity.js'
import { findAll, deleteById, getById, create, update } from './utils.js'

export const getUnities = (req, res) => findAll(Unity, res)

export const deleteUnity = (req, res) => deleteById(Unity, res, req.params.id)

export const getUnityByCompany = (req, res) => getById(Unity, res, { company: req.params.id })

export const createUnity = (req, res) => create(Unity, res, req.body)

export const updateUnity = (req, res) => update(Unity, res, req.body, req.params.id)