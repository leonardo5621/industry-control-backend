import express from 'express'
import { getUnities, createUnity, getUnityByCompany, deleteUnity, updateUnity } from '../controllers/unities.js'

const router = express.Router()

router.get('/', getUnities)
router.post('/', createUnity)
router.get('/:id', getUnityByCompany)
router.patch('/:id', updateUnity)
router.delete('/:id', deleteUnity)

export default router